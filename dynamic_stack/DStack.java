package dynamic_stack;

public class DStack {

    private static int capacity = 2;
    private int stack [] = new int[capacity];
    private int top = 0;

    public void push(int data){
        if(isFull()) {
            expand();
        }
        stack[top++] = data;
    }

    public int pop(){
        int data = 0;
        if (isEmpty())
            System.out.println("Stack is Empty");
        else {
            data = stack[--top];
            stack[top] = 0;
            if (size() < capacity/2) {
                shrink();
            }
        }
        return data;
    }

    public void show(){
        for (int num : stack){
            System.out.print(num + "");
        }
        System.out.println();
    }

    public int peek() {
        int data = 0;

        if (isEmpty()) {
            System.out.println("Stack is Empty");
        }
        else {
            data = stack[top - 1];
        }
        return data;
    }

    public int size() {
        return top;
    }

    public boolean isEmpty() {
        return top <= 0;
    }

    public boolean isFull(){
        return top == capacity - 1;
    }

    public void expand(){
        int len = capacity;
        int newStack [] = new int[capacity*2];
        System.arraycopy(stack, 0, newStack, 0, len);
        stack = newStack;
        newStack = null;
        capacity *= 2;
    }

    public void shrink(){
        int len = size();
        int newStack [] = new int[capacity/2];
        System.arraycopy(stack, 0, newStack, 0, len);
        stack = newStack;
        newStack = null;
        capacity /= 2;
    }
}
