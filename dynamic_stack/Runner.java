package dynamic_stack;

public class Runner {

    public static void main (String ... args) {

        DStack dstack = new DStack();

        dstack.push(10);
        dstack.show();
        dstack.push(20);
        dstack.show();
        dstack.push(30);
        dstack.show();
        dstack.push(40);
        dstack.show();

        dstack.pop();
        dstack.show();
        dstack.pop();
        dstack.show();
        dstack.pop();
        dstack.show();
    }
}
