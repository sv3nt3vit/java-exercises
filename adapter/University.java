package adapter;

public class University {

    public static void main (String ... args) {

        Pen p = new PenAdapter();
        Assignment assignment = new Assignment();
        assignment.setP(p);
        assignment.writeAssignment("Working on this assignment");
    }
}
