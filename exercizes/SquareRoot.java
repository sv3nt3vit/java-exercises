package exercizes;

import java.util.Scanner;

public class SquareRoot {

    public static int floorSqrt(int x) {

        // Base case
        if (x == 0 || x == 1)
            return x;

        // Binary search for floor(sqrt(x))
        int start = 1;
        int end = x;
        int res = 0;

        while (start <= end) {

            int mid = (start + end) / 2;

            // check if x is a perfect square
            if (mid * mid == x)
                return mid;

            // update floor and move closer to answer
            if (mid * mid < x){
                start = mid + 1;
                res = mid;
            }
            else { // mid*mid > x
                end = mid - 1;
            }
        }
        return res;
    }

    public static void main (String ... args) {

        System.out.println("Enter number to get square root : ");
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        System.out.println(floorSqrt(num));
    }
}
