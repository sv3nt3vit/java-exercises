package technotrek;

import org.apache.camel.main.Main;

/**
 * A Camel Application
 */
public class MainApp {
    private int id;

    public MainApp(int id) {
        this.id = id;
    }
    /**
     * A main() so we can easily run these routing rules in our IDE
     */
    public static void main(String... args) throws Exception {
        System.out.println("Hey there technotreck");
        int count = 0;
        MainApp app = new MainApp(count++);
        MainApp app2 = new MainApp(count++);
        System.out.println(app.id);
        System.out.println(app2.id);
    }

}

