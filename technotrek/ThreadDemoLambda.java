package technotrek;

public class ThreadDemoLambda {

    public static void main (String ... args) throws Exception {
        int cores = Runtime.getRuntime().availableProcessors();
        System.out.println("System available cores : " + cores);

        // With Lambda expression can omit new Runnable ... and @Override run()
        Runnable runnable1 = () ->
            {
                for (int i = 0; i < 5; i++) {
                    System.out.println("T1");
                    try {Thread.sleep(500);} catch (Exception e) {}
                }
            }
        ;

        Runnable runnable2 = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 5; i++){
                    System.out.println("T2");
                    try {Thread.sleep(500);} catch (Exception e) {}
                }
            }
        };

        Thread t1 = new Thread(runnable1);
        Thread t2 = new Thread(runnable2);

        t1.start();
        try {Thread.sleep(10);} catch (Exception e) {}
        t2.start();

    }

}
