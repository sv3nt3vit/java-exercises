package technotrek;

public class ThreadDemoLambda2 {

    public static void main (String ... args) throws Exception {
        int cores = Runtime.getRuntime().availableProcessors();
        System.out.println("System available cores : " + cores);

        // Use anonymous classes by moving code from Runnable here
        Thread t1 = new Thread(() ->
        {
            for (int i = 0; i < 5; i++) {
                System.out.println("T1 " + Thread.currentThread().getPriority() );
                try {Thread.sleep(500);} catch (Exception e) {}
            }
        });

        Thread t2 = new Thread(() ->
        {
            for (int i = 0; i < 5; i++) {
                System.out.println("T2");
                try {Thread.sleep(500);} catch (Exception e) {}
            }
        }, "Thread_#_2");

//        t1.setName("Thread_1");
//        t1.setPriority(Thread.MAX_PRIORITY);

        System.out.println(t1.getName());
        System.out.println(t2.getName());

        t1.start();
        try {Thread.sleep(10);} catch (Exception e) {}
        t2.start();

        System.out.println(t1.isAlive());

        // Let main() wait until other threads are finished
        // Otherwise main() thread will intertwine with other threads
        t1.join();
        t2.join();

        System.out.println("Bye");
    }

}
