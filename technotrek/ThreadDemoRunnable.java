package technotrek;

class T3 implements Runnable{
    @Override
    public void run(){
        for (int i = 0; i < 5; i++) {
            System.out.println("T3");
            try {Thread.sleep(500);} catch (Exception e) {}
        }
    }
}

class T4 implements Runnable{
    @Override
    public void run(){
        for (int i = 0; i < 5; i++) {
            System.out.println("T4");
            try {Thread.sleep(500);} catch (Exception e) {}
        }
    }
}

public class ThreadDemoRunnable {

    public static void main (String ... args){
        int cores = Runtime.getRuntime().availableProcessors();
        System.out.println("System available cores : " + cores);
        Runnable t3 = new T3();
        Runnable t4 = new T4();
        Thread t1 = new Thread(t3);
        Thread t2 = new Thread(t4);
        t1.start();
        t2.start();
    }

}
