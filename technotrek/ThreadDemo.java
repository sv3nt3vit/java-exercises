package technotrek;

class T1 extends Thread{
    @Override
    public void run(){
        for (int i = 0; i < 5; i++) {
            System.out.println("T1");
            try {Thread.sleep(500);} catch (Exception e) {}
        }
    }
}

class T2 extends Thread{
    @Override
    public void run(){
        for (int i = 0; i < 5; i++) {
            System.out.println("T2");
            try {Thread.sleep(500);} catch (Exception e) {}
        }
    }
}
public class ThreadDemo {

    public static void main (String ... args){
        int cores = Runtime.getRuntime().availableProcessors();
        System.out.println("System available cores : " + cores);
        T1 t1 = new T1();
        T2 t2 = new T2();
        t1.start();
        t2.start();
    }

}
