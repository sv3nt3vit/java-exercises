package composite;

public class CompositeTest {

    public static void main(String ... args) {

        Component hd = new Leaf(200, "HDD");
        Component mouse = new Leaf(50, "Mouse");
        Component monitor = new Leaf(300, "Monitor");
        Component ram = new Leaf(100, "Ram");
        Component cpu = new Leaf(300, "CPU");

        Composite ph = new Composite("Peripheral");
        Composite cabinet = new Composite("Cabinet");
        Composite motherBoard = new Composite("Motherboard");
        Composite computer = new Composite("PC");

        ph.addComponent(mouse);
        ph.addComponent(monitor);

        motherBoard.addComponent(ram);
        motherBoard.addComponent(cpu);

        cabinet.addComponent(hd);
        cabinet.addComponent(motherBoard);

        computer.addComponent(cabinet);
        computer.addComponent(ph);

        ram.showPrice();
        cpu.showPrice();
        monitor.showPrice();

        motherBoard.showPrice();

        computer.showPrice();
    }
}
