package linkedlist;

public class LinkedList {

    Node head;

    public void insert (int data) {
        Node newNode = new Node();
        newNode.data = data;
        newNode.next = null;

        if (head == null){
            head = newNode;
        }
        else{
            Node curr = head;
            while (curr.next != null){
                curr = curr.next;
            }
            curr.next = newNode;
        }

    }

    public void insertAt(int index, int data){

        Node newNode = new Node();
        newNode.data = data;
        newNode.next = null;

        if (index == 0){
            insertAtStart(data);
        }
        else {
            // Anytime we want to traverse start with this
            Node temp = head;
            for (int i = 0; i < index - 1; i++) {
                temp = temp.next;
            }

            newNode.next = temp.next;
            temp.next = newNode;
        }
    }

    public void insertAtStart(int data){

        Node newNode = new Node();
        newNode.data = data;
        newNode.next = head;

        head = newNode;

    }

    public void show(){

        Node node = head;
        while(node.next != null){
            System.out.println(node.data);
            node = node.next;
        }

        // Last node points to null, so we need to print data manually
        System.out.println(node.data);
    }

    public void deleteAt(int index){

        if (index == 0){
            head = head.next;
        }
        else {
            Node temp = head;
            Node temp2 = null;
            for (int i = 0; i < index - 1; i++) {
                temp = temp.next;
            }
            temp2 = temp.next;
            temp.next = temp2.next;

            // Eligible for GC
            temp2 = null;
        }
    }
}
