# README #

Each package contains independent programs and drivers

### What is this repository for? ###

Excercises in Java for Data Structures, Algorithms, Multhithreading and Design Patterns along with some coding problems.

### How do I get set up? ###

* Have Java 8 set up locally
* Clone the repo or copy/paste specific example and run
* Some examples may require additional trivial drivers

