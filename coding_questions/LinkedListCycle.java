package coding_questions;

class Node {
    int data;
    Node next;
}
public class LinkedListCycle {

    boolean hasCycle(Node head) {
        if (head == null) return false;

        Node fast = head.next; //start at different nodes, otherwise equal
        Node slow = head;

        while (fast != null && fast.next != null && slow != null) {
            if (fast == slow) {
                return true;
            }
            fast = fast.next.next; // move by 2
            slow = slow.next.next; // move by 1
        }
        return false;
    }
}
