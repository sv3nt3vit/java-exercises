package stack;

public class Stack {

    private static final int MAXSIZE = 5;
    private int stack [] = new int[MAXSIZE];
    private int top = 0;

    public void push(int data){
        if(isFull()){
            System.out.println("Stack is Full");
        } else {
            stack[top++] = data;
        }
    }

    public int pop(){
        int data = 0;
        if (isEmpty())
            System.out.println("Stack is Empty");
        else {
            data = stack[--top];
            stack[top] = 0;
        }
        return data;
    }

    public void show(){
        for (int num : stack){
            System.out.println(num + "");
        }
    }


    public int peek() {
        if (isEmpty())
            return 0;
        return stack[top-1];
    }

    public int size() {
        return top;
    }

    public boolean isEmpty() {
        return top <= 0;
    }

    public boolean isFull(){
        return top == MAXSIZE - 1;
    }
}
