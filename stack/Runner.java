package stack;

public class Runner {

    public static void main (String ... args) {
        Stack s = new Stack();
        s.push(15);
        s.push(25);
        s.push(35);
        s.show();

        s.pop();
        s.show();
        System.out.println(s.peek());
        System.out.println(s.peek());
        System.out.println(s.size());
        System.out.println(s.isEmpty());
        System.out.println(s.isFull());

        s.push(20);
        s.push(20);
        s.push(20);
        s.push(20);


    }
}
