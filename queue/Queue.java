package queue;

public class Queue {

    private int queueSize = 5;
    private int queue [] = new int[queueSize];
    private int size;
    private int front;
    private int rear;

    public void enqueue(int data){

        if(isFull())
            System.out.println("Error : Queue is Full.");
        else {
            queue[rear] = data;
            rear = (rear + 1) % queueSize;
            size++;
        }
    }

    public int dequeue(){
        int data = 0;

        if (isEmpty())
            System.out.println("Error : Queue is Empty.");
        else {
            data = queue[front];
            front = (front + 1) % queueSize;
            size--;
        }
        return data;
    }

    public void show(){

        System.out.print("Elements : ");
        for (int i = 0; i < size; i++){
            System.out.print(queue[ (front+i) % queueSize ] + " ");
        }
        System.out.println();
    }

    public int getSize(){
        return size;
    }

    public boolean isEmpty(){
        return getSize() == 0;
    }

    public boolean isFull(){
        return getSize() == queueSize;
    }

}
