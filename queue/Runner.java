package queue;

public class Runner {

    public static void main (String ... args) {

        Queue queue = new Queue();

        queue.dequeue();

        queue.enqueue(10);
        queue.enqueue(20);
        queue.enqueue(30);
        queue.enqueue(40);

        queue.dequeue();

        queue.show();

        queue.dequeue();
        queue.show();
        queue.dequeue();
        queue.show();

        System.out.println(queue.getSize());
        System.out.println(queue.isEmpty());
        System.out.println(queue.isFull());

        queue.enqueue(50);
        queue.enqueue(60);
        queue.enqueue(60);
        queue.enqueue(90);
        queue.enqueue(100);

    }
}
