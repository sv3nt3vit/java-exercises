package search;

/*
* Input: array sorted in asc order, left & right search boundaries
* Output: index of the element if found, otherwise -1
* */

public class BinarySearch {

    public static int binarySearchRecursion(int [] nums, int val, int left, int right) {

        if (left >= right) return nums[left] == val ? left : -1;
        int mid = (left+right)/2;
        if (nums[mid] == val) return mid;
        if (nums[mid] < val){
            return binarySearchRecursion(nums, val, left, mid-1);
        } else{
            return binarySearchRecursion(nums, val, mid+1, right);
        }
    }

    public static int binarySearchIteration(int [] nums, int val, int left, int right) {

        if (left >= right) return nums[left] == val ? left : -1;
        while (left <= right) {
            int mid = (left + right) / 2;
            if (nums[mid] == val) return mid;
            if (nums[mid] < val) {
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        return val;
    }

    public static void main (String ... args) {

        int arr[] = {1,2,3,4,5,6,7,8,9};

        int idx = binarySearchRecursion(arr, 6, 1, 9);
        System.out.println(idx);

        int idx2 = binarySearchIteration(arr, 6, 1, 9);
        System.out.println(idx2);

    }
}
